


import { Component, OnInit } from '@angular/core';
import {EmpService} from '../emp.service';;
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user:any;
  email : any;
  password:any;
  c:any;
  constructor(private service:EmpService, private router: Router) { 
    this.email='';
    this.password='';
  }

  ngOnInit(): void {
    this.service.fetchDetailsuser().subscribe((result:any)=>{
      this.user=result;
    });
  }
  loginSubmit2(loginForm : any) : void{
    this.user.forEach((u : any) => {

      if(u.email === loginForm.email && u.password === loginForm.password){
        this.c=1;
        this.router.navigate(['../user']);
        //alert("Welcome to  Home page "+u.username);
      }
      if(loginForm.email === "employee@gmail.com" && loginForm.password === "employee"){
        this.c=1;
        this.router.navigate(['../employeehomepage']);
        //alert("Welcome to employee page ");
      }
    })
    if(this.c!=1){alert("Invalid LOGIN");} 
  }
}
