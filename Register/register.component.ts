import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  CountriesList: any;
  booking: any;
  user: any;
  form: any;
  constructor(private httpClient: HttpClient,private service: EmpService){//private httpClient: HttpClient) { 
    //this.employee = {empId:'',empName:'',salary:'',gender:'',email:'',password:'',doj:'',mobileno:'',country:''};
    this.user = {pet:'',phone:'',from:'',email:'',to:'',note:''};
  }

  ngOnInit(){
  }
  register(){
    //this.service.registerEmp(this.employee).subscribe((result: any)=>console.log(result));
    this.service.registerEmp(this.user).subscribe((result: any)=>console.log(result));
  }
  regSubmit(): void{
    console.log(this.form);
  }
  registerForm(): void{
    console.log("Name"+this.form.pet);
    console.log("Phone Number"+this.form.phone);
    console.log("from "+this.form.from);
    console.log("Email"+this.form.email);
    console.log("to "+this.form.to);
    console.log("note "+this.form.note);
  }
  registerform1(regform: any): void{
    console.log(regform);
  }
}
