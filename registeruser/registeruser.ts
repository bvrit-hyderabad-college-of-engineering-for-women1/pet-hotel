import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-registeruser',
  templateUrl: './registeruser.component.html',
  styleUrls: ['./registeruser.component.css']
})
export class RegisteruserComponent implements OnInit {
  CountriesList: any;
  booking: any;
  user: any;
  form: any;
  constructor(private httpClient: HttpClient,private service: EmpService){//private httpClient: HttpClient) { 
    //this.employee = {empId:'',empName:'',salary:'',gender:'',email:'',password:'',doj:'',mobileno:'',country:''};
    this.user = {username:'',phone:'',email:'',password:''};
  }

  ngOnInit(){
  }
  registeruser(){
    //this.service.registerEmp(this.employee).subscribe((result: any)=>console.log(result));
    this.service.registeruser(this.user).subscribe((result: any)=>console.log(result));
  }
  regSubmit(): void{
    console.log(this.form);
  }
  registerForm(): void{
    console.log("Username"+this.form.username);
    console.log("Phone Number"+this.form.phone);
    console.log("Email Id : "+this.form.email);
    console.log("Password"+this.form.password);
  }
  registerform1(regform: any): void{
    console.log(regform);
  }
  

}

