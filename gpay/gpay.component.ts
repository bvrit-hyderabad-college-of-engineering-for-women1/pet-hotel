import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gpay',
  templateUrl: './gpay.component.html',
  styleUrls: ['./gpay.component.css']
})

export class GpayComponent{
  title = 'demo';
  buttonType="buy";
  buttonColor="black";
  isCustomSize=300;
  buttonHeight=50;
  isTop = window===window.top;
  paymentRequest={
    apiVersion: 2,
    apiVersionMinor: 0,
    allowedPaymentMethods: [
      {
        type: 'CARD',
        parameters: {
          allowedAuthMethods: ['PAN_ONLY', 'CRYPTOGRAM_3DS'],
          allowedCardNetworks: ['AMEX', 'VISA', 'MASTERCARD']
        },
        tokenizationSpecification: {
          type: 'PAYMENT_GATEWAY',
          parameters: {
            gateway: 'example',
            gatewayMerchantId: 'exampleGatewayMerchantId'
          }
        }
      }
    ],
    merchantInfo: {
      merchantId: '935740470949-sq77ce02bjeaujj9j33jvrfv2a2k329j.apps.googleusercontent.com',
      merchantName: 'Demo Merchant'
    },
    transactionInfo: {
      totalPriceStatus: 'FINAL',
      totalPriceLabel: 'Total',
      totalPrice: '1500',
      currencyCode: 'USD',
      countryCode: 'US'
    },
  };

  onLoadPaymentData(event:any):void{
   console.log("oad Payment Data by Testycodeoz",event.detail)
  }

}

